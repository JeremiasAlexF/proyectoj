from django.urls import include, path
from .views import Home, Formulario
from rest_framework import routers
from usuarios import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


urlpatterns = [
    path('', Home, name = "Home"),
    path('Formulario/', Formulario, name = "Formulario"),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
