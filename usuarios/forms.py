from django import forms
from .models import Comprador

class CompradorForm(forms.ModelForm):

    class Meta:
        model = Comprador
        fields =('Nombre','Rut','Correo','Telefono',
                 'Fecha','ProductoFav','Puntuacion','Direccion')
        
