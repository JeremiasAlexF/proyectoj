$(function(){
  $("#formulario").validate({
    rules: {
      nombre:{
        required:true
      },
      rut:{
        required:true
      },
      correo:{
        required:true
      },
      fono:{
        required:true
      },
      fecha:{
        required:true
      },
      comida:{
        required:true
      },
      estado:{
        required:true
      },
      version:{
        required:true
    },
    messages:{
      nombre:{
        required:'debe ingresar el nombre!',
        minlength:'5 caractere minimo'
      },
      rut:{
        required:'debe ingresar el rut!',
        minlength:'8 caractere minimo',
        maxlegth: "10 carracte maximo"
      },
      correo:{
        required:'debe ingresar el correo!',
      },
      fono:{
        required:'debe ingresar el telefono!',
        minlength:'9 digito'
      },

      comida:{
        required:'debe ingresar la comida!'

      },
      fecha:{
        required:'debe ingresar la fecha!'
      },
      version:{
        required:'debe ingresar version!'
      },
      estado:{
        required:'debe ingresar estado!',

    }
  });
});
